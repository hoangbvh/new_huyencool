package huyencool.com.music2;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import huyencool.com.music2.model.BaiHat;

public class MediaPlaybackService extends Service implements MediaPlayer.OnCompletionListener {
    MediaPlayer mediaPlayer;

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }


    @Override
    public void onCreate() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String song = intent.getStringExtra("song");//nhan nhac tu AllSongs sang
        String action = intent.getStringExtra("action");
        int timeTo = intent.getIntExtra("timeTo",0);
        if (song != null && action != null && !action.equals("") && !song.equals("")) {
            Uri uri = Uri.parse(song);
            switch (action) {
                case "new":
                    mediaPlayer = MediaPlayer.create(this, uri);// raw/s.mp3 // khoi tao de bat dau phat bai co ID(tieng nhac)la song
                    mediaPlayer.setOnCompletionListener(this);
                    mediaPlayer.start();
                    break;
                case "pause":
                    if (mediaPlayer != null)
                    mediaPlayer.pause();
                    break;
                case "play":
                    if (mediaPlayer != null)
                    mediaPlayer.start();
                    break;
                case "stop":
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                    break;

                case "goto":
                    if (mediaPlayer != null)
                    mediaPlayer.seekTo(timeTo);
                    break;
                default:
                    if (mediaPlayer != null)
                        sendBroadcast(mediaPlayer.getCurrentPosition(), mediaPlayer.getDuration());
                    break;
            }

        }


        return START_STICKY;
    }

    private void sendBroadcast (int currentTime, int maxTime){
        Intent intent = new Intent ("message"); //put the same message as in the filter you used in the activity when registering the receiver
        intent.putExtra("currentTime", currentTime + "");
        intent.putExtra("maxTime", maxTime + "");

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
    public void onDestroy() {
        if (mediaPlayer != null){
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
        }

    }

    public void onCompletion(MediaPlayer _mediaPlayer) {
        stopSelf();
    }


}