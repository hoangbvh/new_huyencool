package huyencool.com.music2.view;

import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import huyencool.com.music2.R;
import huyencool.com.music2.model.BaiHat;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MusicItemViewHolder> {
    ArrayList <BaiHat> baiHats;
    private OnClickRecyclerViewItem listener;
    int positionActive;
    RecyclerViewAdapter(){

    }
    RecyclerViewAdapter(ArrayList<BaiHat> baiHats,int positionActive, OnClickRecyclerViewItem listener)
    {
        this.positionActive = positionActive;
        this.listener = listener;
        this.baiHats = baiHats;
    }
    @NonNull
    @Override
    public MusicItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_music,parent,false);
        return new RecyclerViewAdapter.MusicItemViewHolder(view);
    }
    private TextView textViewOldIndex;
    private TextView textViewOldName;
    private String oldIndexValue;

    @Override
    public void onBindViewHolder(@NonNull final MusicItemViewHolder holder, final int position) {
        BaiHat baihat = baiHats.get(position);
        holder.mTime.setText(baihat.getmTime());
        if (position == positionActive){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                holder.mStt.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_queue_dark,0,0,0);
            }
            holder.mTenBaiHat.setTypeface(Typeface.DEFAULT, Typeface.BOLD); // dang click
            textViewOldName = holder.mTenBaiHat;
            textViewOldIndex = holder.mStt;
            oldIndexValue = position + 1 + "";
        }
        else{
            holder.mTenBaiHat.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                holder.mStt.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,0,0);
            }
            holder.mStt.setText(baihat.getmStt());
            holder.mTenBaiHat.setText(baihat.getmTenbaihat());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // lần click đầu tiên thi chưa có 2 biến oldName va old Index nên dòng  if này không thực hiện
                // sau khi click thì gán luôn cái textView tên bài hát và TextView số tt cho 2 biến old này
                // thứ tự thực hiện hàm onclick này là :
                // 1. gọi click sang bên hàm main chính là câu lệnh   listener.onItemClickListener(position);
                // 2. kiểm tra nếu có oldIndex và oldName thì đặt lại oldIndex và đặt lại tên thành chữ thường
                // 3. đặt icon cho index vừa mới click và đặt chữ đậm cho tên mới click
                // 4. gán lại 2 biến old cho lần gọi tiếp theo

                // clicl vao dong 1 thì ten của dòng 1 Bold và thay dổi icon của index
                // sau đó gán các giá trị cho các biến old cụ thể là lúc này oldIndex = TextView mStt và textViewOldName = mtenbaihat (2 biến này là textView nên sử dụng các sự kiện như textView bình thường)

                if (textViewOldIndex != null && textViewOldName != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        textViewOldIndex.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,0,0);
                    }
                    textViewOldIndex.setText((oldIndexValue + ""));
                    textViewOldName.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    holder.mStt.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_queue_dark,0,0,0);
                }
                holder.mTenBaiHat.setTypeface(Typeface.DEFAULT, Typeface.BOLD); // dang click

                // luu ten va chi so cua dong day cho cac bien oldName va oldIndex
                textViewOldName = holder.mTenBaiHat;
                textViewOldIndex = holder.mStt;
                oldIndexValue = position + 1 + "";
                listener.onItemClickListener(position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return baiHats.size();
    }

    class MusicItemViewHolder extends RecyclerView.ViewHolder {

        TextView mStt;
        TextView mTenBaiHat;
        TextView mTime;
        MusicItemViewHolder(@NonNull View itemView) {
            super(itemView);
            mStt=itemView.findViewById(R.id.index);
            mTenBaiHat=itemView.findViewById(R.id.musicName);
            mTime=itemView.findViewById(R.id.time);
            //mAnh=itemView.findViewById(R.id.more);
        }
    }
}
//in
// SOLID