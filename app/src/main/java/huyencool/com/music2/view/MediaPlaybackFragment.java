package huyencool.com.music2.view;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import huyencool.com.music2.Library;
import huyencool.com.music2.MyNotification;
import huyencool.com.music2.R;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.model.FavoriteSongsProvider;
import huyencool.com.music2.view.new_interface.CallBackHelper;
import huyencool.com.music2.view.new_interface.MusicPlayListener;
import huyencool.com.music2.view.new_interface.ViewPlayerListener;



public class MediaPlaybackFragment extends Fragment implements CallBackHelper {
    BaiHat baiHatActive;
    ArrayList<BaiHat> listBaiHat;
    TextView nameMusic;
    TextView artist;
    ImageView smallImageMusic;
    ImageView bigImageMusic;
    ImageView nextMusicButton;
    ImageView backToListButton;
    SeekBar seekBar;
    TextView timeBatdau;
    TextView timeKetthuc;
    ImageView pausePlayButtom;
    ImageView playButton;       //button play in header
    ImageView prevButton;
    ImageView likeButton;
    ImageView disLikeButton;
    ImageView moreButton;
    ImageView repeatButton;
    ImageView shuffleButton;
    int sTime = 0;
    int eTime = 20000000;
    boolean isPlaying = true;
    int positionActive = 0;
    int repeatType = 0;
    Boolean isShuffle = false;
    //
    Boolean isFavorite = false;
    Boolean isDisLike = false;
    LinearLayout viewPlayButton;
    LinearLayout viewBackAndMoreButton;
    //
    ConstraintLayout headerMediaPlayer;
    ViewPlayerListener viewPlayerListener;

    //
    MusicPlayListener musicPlayListener;
    public void setListBaiHat(ArrayList<BaiHat> list){
        this.listBaiHat = list;
    }
    public void setPositionActive(int positionActive){
        this.positionActive = positionActive;
    }
    public void setMusicPlayListener(MusicPlayListener musicPlayListener){
        this.musicPlayListener = musicPlayListener;
    }

    public void setViewPlayerListener(ViewPlayerListener viewPlayerListener){
        this.viewPlayerListener = viewPlayerListener;
    }
    public MediaPlaybackFragment(){

    }
    public void setBaiHatActive(BaiHat baiHatActive){
        this.baiHatActive = baiHatActive;
    }
    private BroadcastReceiver bReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
           // sTime = Integer.parseInt(intent.getStringExtra("currentTime"));
            eTime = Integer.parseInt(intent.getStringExtra("maxTime"));

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    seekBar.setProgress(sTime/1000);
                    seekBar.setMax(eTime/1000);
                    long phutKetthuc = TimeUnit.MILLISECONDS.toMinutes((long) eTime);
                    long giayKetthuc = TimeUnit.MILLISECONDS.toSeconds((long) eTime) - (phutKetthuc * 60);
                    timeKetthuc.setText(String.format("%d : %d", phutKetthuc, giayKetthuc));
                }
            });
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(bReceiver, new IntentFilter("message")); // dki vs service
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(bReceiver); // huy dki service
        isPlaying = false;
        isChangeSeekBar = false;
    }
    Boolean isChangeSeekBar = false;
    private void changeSeekBar(int s){
        if (!isChangeSeekBar)
            return;
        seekBar.setProgress(sTime / 1000); // hien thi time chay
        if (sTime/1000 >= eTime/1000) {
            sTime = 0;
            if (isShuffle){     // neu dang duoc đặt là xáo trộn bài hát (ngẫu nhiên)   thì random position
                final int min = 0;
                final int max = listBaiHat.size() - 1;
                final int randomPosition = new Random().nextInt((max - min) + 1) + min;
                seekBar.setProgress(0);
                positionActive = randomPosition;
                updateUI(listBaiHat.get(positionActive));
                musicPlayListener.onNext(positionActive);
            }else{
                if (repeatType == 1){       // nếu click lặp bài hát hiện tại
                    musicPlayListener.start(positionActive);
                    seekBar.setProgress(0);
                }
                else if(repeatType == 2){       // truong hop lap lai tat ca
                    seekBar.setProgress(0);
                    int p = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
                    positionActive = p;
                    updateUI(listBaiHat.get(p));
                    musicPlayListener.onNext(positionActive);
                }
                else{           // truong hop ko lap la thi kiểm tra nếu bài hát đang hát ở cuối danh sách thì ko phát lại nữa
                    if (positionActive != listBaiHat.size() -1){
                        int p = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
                        positionActive = p;
                        updateUI(listBaiHat.get(p));
                        musicPlayListener.onNext(positionActive);
                    }
                    else{
                        musicPlayListener.stop();
                    }
                }
            }
        }
        else{
            sTime = s + 1000;
            // gán lại thời gian của bài hát đang hát bi dung
            final int tmp = s + 1000;
            final long phutBatdau = TimeUnit.MILLISECONDS.toMinutes((long) sTime);
            final long giayBatdau = TimeUnit.MILLISECONDS.toSeconds((long) sTime) - (phutBatdau * 60);
            new Handler().postDelayed(new Runnable() { // hàm chơ 1 giây sau thực hiện tiếp
                @Override
                public void run() {
                    if (tmp == sTime){
                        timeBatdau.setText(String.format("%d : %d", phutBatdau, giayBatdau));
                    }
                    changeSeekBar(sTime);
                }
            },1000);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_media_playback2, container, false);
         nameMusic = view.findViewById(R.id.musicName);
         artist = view.findViewById(R.id.musicArtist);
         smallImageMusic = view.findViewById(R.id.smallImageMusic);
         bigImageMusic = view.findViewById(R.id.musicBigImageView);
         seekBar = view.findViewById(R.id.seekBar);
         timeBatdau = view.findViewById(R.id.startTimeTextView);
         timeKetthuc = view.findViewById(R.id.endTimeTextView);
         pausePlayButtom = view.findViewById(R.id.pausePlayButton);
         nextMusicButton = view.findViewById(R.id.nextMusicButton);
         prevButton = view.findViewById(R.id.prevButton);
         likeButton = view.findViewById(R.id.likeButton);
         disLikeButton = view.findViewById(R.id.disLikeButton);
         moreButton = view.findViewById(R.id.moreButton);
         repeatButton = view.findViewById(R.id.repeatButton);
         shuffleButton = view.findViewById(R.id.shuffleButton);
         // header view
        viewBackAndMoreButton = view.findViewById(R.id.viewBackAndMoreButton);
        viewPlayButton = view.findViewById(R.id.viewPlayButton);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            viewBackAndMoreButton.setVisibility(LinearLayout.GONE);
            viewPlayButton.setVisibility(View.VISIBLE);
        }
        else{
            viewBackAndMoreButton.setVisibility(LinearLayout.VISIBLE);
            viewPlayButton.setVisibility(View.GONE);
        }
        //changeSeekBar(sTime);

        likeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                FavoriteSongsProvider favoriteSongsProvider = new FavoriteSongsProvider();
                if (isFavorite){
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_default);
                }
                else{
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_selected);
                }
                if (isDisLike){
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_default);
                }
                isFavorite = !isFavorite;
                    // luu lai danh sach bai hat vao bộ nhớ bài yêu thích và không yêu thích
            }
        });

        disLikeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (isDisLike){
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_default);
                }
                else{
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_selected);
                }
                if (isFavorite){
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_default);
                }
                isFavorite = !isFavorite;
                // luu lai danh sach bai hat vao bộ nhớ bài yêu thích và không yêu thích
            }
        });
        moreButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getContext(),view);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.listFavorite:
                                return true;
                            case R.id.listDislike:
                            default:
                                return false;
                        }
                    }
                });
                MenuInflater menuInflater = popupMenu.getMenuInflater();
                menuInflater.inflate(R.menu.menu_media, popupMenu.getMenu());
                popupMenu.show();
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                musicPlayListener.seekTo(baiHatActive,seekBar.getProgress() * 1000);
                sTime = seekBar.getProgress() * 1000;
                final long phutBatdau = TimeUnit.MILLISECONDS.toMinutes((long) sTime);
                final long giayBatdau = TimeUnit.MILLISECONDS.toSeconds((long) sTime) - (phutBatdau * 60);
                timeBatdau.setText(String.format("%d : %d", phutBatdau, giayBatdau));

            }
        });
         //header media player
        headerMediaPlayer = view.findViewById(R.id.headerMediaPlayer);
        headerMediaPlayer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                viewBackAndMoreButton.setVisibility(LinearLayout.VISIBLE);
                viewPlayButton.setVisibility(View.GONE);
                viewPlayerListener.onTouchToOpen();
            }
        });
        //
        backToListButton = view.findViewById(R.id.backToListButton);
        backToListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewBackAndMoreButton.setVisibility(LinearLayout.GONE);
                viewPlayButton.setVisibility(View.VISIBLE);
                viewPlayerListener.onCollapseView();
            }
        });
        playButton = view.findViewById(R.id.playButton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateState();

            }
        });

         pausePlayButtom.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 updateState();
             }
         });
         nextMusicButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 seekBar.setProgress(0);
                 if (isShuffle){     // neu dang duoc đặt là xáo trộn bài hát (ngẫu nhiên)   thì random position
                     final int min = 0;
                     final int max = listBaiHat.size() - 1;
                     final int randomPosition = new Random().nextInt((max - min) + 1) + min;
                     positionActive = randomPosition;
                 }
                 else{
                     positionActive = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
                 }
                 updateUI(listBaiHat.get(positionActive));
                 musicPlayListener.onNext(positionActive);
             }
         });
        prevButton.setOnClickListener(new View.OnClickListener( ){
            @Override
            public void onClick(View view) {
                if (sTime > 3000){
                    sTime = 0;
                    musicPlayListener.seekTo(baiHatActive, 0);
                    seekBar.setProgress(0);
                    timeBatdau.setText(String.format("%d : %d", 0, 0));
                }else{
                    int p = (positionActive == 0) ? listBaiHat.size() - 1 : positionActive - 1;
                    positionActive = p;
                    updateUI(listBaiHat.get(p));
                    musicPlayListener.onPrev(positionActive);
                }
            }
        });

        //click lap lai bai hat
        repeatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (repeatType == 0){
                    repeatType = 1;
                }
                else if (repeatType == 1){
                    repeatType = 2;
                }
                else repeatType = 0;
                updateRepeatIcon(repeatType);
            }
        });
        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isShuffle = !isShuffle;
                updateShuffleIcon(isShuffle);

            }
        });
        isChangeSeekBar = true;
        changeSeekBar(sTime);
        Log.d("sadasa","ok chua");
        if (baiHatActive != null)
        updateUI(baiHatActive);
        return view;
    }


    private static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void updateShuffleIcon(Boolean isShuffle){
        if (isShuffle){
            shuffleButton.setImageResource(R.drawable.ic_play_shuffle_orange);
        }
        else{
            shuffleButton.setImageResource(R.drawable.ic_play_shuffle_off);
        }
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, Activity.MODE_PRIVATE).edit();
        editor.putBoolean("isShuffle",isShuffle);
        editor.apply();
    }
    private void updateRepeatIcon(int repeatType){
        switch (repeatType){
            case 0: // ko lap lai
                repeatButton.setImageResource(R.drawable.ic_repeat_off);
                break;
            case 1: //lap lai 1 bai
                repeatButton.setImageResource(R.drawable.ic_repeat_one);
                break;
            case 2: //lap lai tat ca
                repeatButton.setImageResource(R.drawable.ic_repeat_all);
                break;
        }
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(MY_PREFS_NAME, Activity.MODE_PRIVATE).edit();
        editor.putInt("repeatType",repeatType);
        editor.apply();
    }
    private void updateState(){
        MyNotification.createNotification(getContext(),baiHatActive,!isPlaying);
        if(isPlaying){
            isChangeSeekBar= false;
            musicPlayListener.onPause(baiHatActive);
            playButton.setImageResource(R.drawable.ic_play_black_round);
            pausePlayButtom.setImageResource(R.drawable.ic_play_black_round);
            isPlaying = false;
        }
        else{
            isPlaying = true;
            isChangeSeekBar = true;
            musicPlayListener.onPlay(baiHatActive);
            changeSeekBar(sTime);
            playButton.setImageResource(R.drawable.ic_pause_black_large);
            pausePlayButtom.setImageResource(R.drawable.ic_pause_black_large);
        }
    }
    public void updateUI(BaiHat baiHat){
        timeBatdau.setText(String.format("%d : %d", 0, 0));
        sTime = 0;
        baiHatActive = baiHat;
        viewPlayerListener.setMusicActive(listBaiHat,baiHat,positionActive); // đặt lại bài hát đang chọn cho allsongfragment để in đậm tên bài, thay icon, gọi lại khi xoay ngang...
        MyNotification.createNotification(getContext(),baiHatActive,isPlaying);
        musicPlayListener.onGetTime(baiHat);
        smallImageMusic.setImageBitmap(Library.StringToBitMap(baiHat.getAnh()));
        nameMusic.setText(baiHat.getmTenbaihat());
        artist.setText(baiHat.getmTencasi());
        bigImageMusic.setImageBitmap(Library.StringToBitMap(baiHat.getAnh()));
        if (!isChangeSeekBar){
            changeSeekBar(sTime);
        }
        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME,  Activity.MODE_PRIVATE);
        repeatType = prefs.getInt("repeatType", 0);     //default = 0
        isShuffle = prefs.getBoolean("isShuffle", false);   //default = false
        updateRepeatIcon(repeatType);
        updateShuffleIcon(isShuffle);
    }

    @Override
    public void callBack(BaiHat baiHat,int pos, String action) {
        setBaiHatActive(baiHat);
        positionActive = pos;
        viewPlayerListener.setMusicActive(listBaiHat,baiHat,positionActive); // đặt lại bài hát đang chọn cho allsongfragment để in đậm tên bài, thay icon, gọi lại khi xoay ngang...
        if(action != null){
            switch (action) {
                case "prev": {
                    updateUI(baiHat);
                    musicPlayListener.onPrev(positionActive);
                    break;
                }
                case "next": {
                    updateUI(baiHat);
                    musicPlayListener.onNext(positionActive);
                    break;
                }
                case "pause":
                    isPlaying = true;
                    updateState();
                    break;
                case "play":
                    isPlaying = false;
                    updateState();
                    break;
            }
        }
    }
}
