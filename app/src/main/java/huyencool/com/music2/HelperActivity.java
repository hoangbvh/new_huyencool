package huyencool.com.music2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.view.new_interface.CallBackHelper;

public class HelperActivity extends BroadcastReceiver {
    public static CallBackHelper callBackHelper;
    public static ArrayList<BaiHat> listBaiHat;
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Bundle extras = intent.getExtras();
        if(extras!=null){
            int pos = extras.getInt("pos");
            callBackHelper.callBack(listBaiHat.get(pos),pos,action); // goi ve mediaplaybackfragment thuc thi
        }
    }


}
