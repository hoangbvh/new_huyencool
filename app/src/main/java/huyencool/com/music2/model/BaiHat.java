package huyencool.com.music2.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class BaiHat implements Serializable {
    String mStt;
    String mTenbaihat;
    String mTencasi;
    String mTime;
    String uri;
    String anh;

    public BaiHat()
    {
        super();
    }
    public BaiHat(String mStt, String mTenbaihat, String mTencasi, String mTime, String uri,String anh)
    {
        super();
        this.mStt=mStt;
        this.mTenbaihat=mTenbaihat;
        this.mTencasi=mTencasi;
        this.mTime=mTime;
        this.uri=uri;
        this.anh=anh;
    }

    protected BaiHat(Parcel in) {
        mStt = in.readString();
        mTenbaihat = in.readString();
        mTencasi = in.readString();
        mTime = in.readString();
        uri = in.readString();
        anh = in.readString();
    }

    public String getmStt()
    {
        return mStt;
    }
    public String  getmTenbaihat()
    {
        return mTenbaihat;
    }
    public String getmTencasi()
    {
        return mTencasi;
    }

    public String getmTime()
    {
        return mTime;
    }
    public String getAnh()
    {
        return anh;
    }
    public String getUri()
    {
        return uri;
    }
}

