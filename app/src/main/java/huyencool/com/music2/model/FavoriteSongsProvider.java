package huyencool.com.music2.model;

import android.annotation.SuppressLint;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class FavoriteSongsProvider extends ContentProvider {
    static final String PROVIDER_NAME = "huyencool.com.music2.model.MyProvider";
    static final String URL = "content://" + PROVIDER_NAME + "/cte";
    static final Uri CONTENT_URI = Uri.parse(URL);
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase mDatabase;
    @Override
    public boolean onCreate() {
        databaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    //  strings là column select
    //  s là whereClause = "column1 = ? OR column1 = ?";
    //  strings1 là mảng giá trị của where
    //  s1 order by
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {
        mDatabase = databaseHelper.getReadableDatabase();
        return mDatabase.query("FavoriteSongsProvider",strings,s,strings1,null,null,s1);
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        mDatabase = databaseHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
//        cv.put(columnName1, value);
//        cv.put(columnName2, value);

        long rowID = mDatabase.insert("FavoriteSongsProvider",null,cv);
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        mDatabase = databaseHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
//        cv.put(columnName1, value);
//        cv.put(columnName2, value);
        String whereClause = "id = ?";          //where id = ?
        String whereArgs[] = {"1"};             // = 1 , gia tri cho where id = 1
        mDatabase.update("FavoriteSongsProvider", cv, whereClause, whereArgs);
        return 0;
    }
}
