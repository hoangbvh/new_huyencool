package huyencool.com.music2.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    final static String TABLE_NOTE =  "FavoriteSongsProvider";
    DatabaseHelper(Context context){
        super(context,TABLE_NOTE,null,1);

    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("mdada","tao bang");
        String script = "CREATE TABLE FavoriteSongsProvider ( ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ID_PROVIDER INTEGER, ID_FAVORITE INTEGER, COUNT_OF_PLAY INTEGER)";
        sqLiteDatabase.execSQL(script);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop table
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS FavoriteSongsProvider");
        onCreate(sqLiteDatabase);
    }
}
