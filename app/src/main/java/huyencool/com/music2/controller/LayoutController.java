package huyencool.com.music2.controller;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import huyencool.com.music2.MediaPlaybackService;
import huyencool.com.music2.MyNotification;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.view.AllSongFragment;
import huyencool.com.music2.view.MediaPlaybackFragment;
import huyencool.com.music2.view.new_interface.AllSongView;
import huyencool.com.music2.view.new_interface.ListMusicListener;
import huyencool.com.music2.view.new_interface.MusicPlayListener;
import huyencool.com.music2.view.new_interface.ViewPlayerListener;

public abstract class LayoutController implements MusicPlayListener, ListMusicListener, AllSongView { // abstract class là lớp trừu tượng bao gồm những thuộc tính và phương thức chung của 2 View cần hiển thị là 2 fragment , no xu li su kien ( click, tat ca tuong tac ng dung tren man hinh)
    BaiHat baiHatActive;
    ArrayList<BaiHat> listBaiHat;
    AppCompatActivity mActivity;      // tham số là activity của main
    ViewPlayerListener viewPlayerListener;
    AllSongFragment allSongFragment;
    MediaPlaybackFragment mediaPlaybackFragment;
    LayoutController(AppCompatActivity activity){
        this.mActivity = activity;
    }
    public abstract void onSaveInstanceState(Bundle outState);
    public abstract void onCreate(Bundle saveInstanceState, String title);      // hàm khởi tạo view
    void startService(BaiHat baiHatActive)
    {
        mActivity.stopService(new Intent(mActivity,MediaPlaybackService.class));//
        onActionService("new",baiHatActive);
    }

    private void onActionService(String action, BaiHat baiHat){
        baiHatActive = baiHat;
        Intent intent = new Intent(mActivity, MediaPlaybackService.class);  // khoi tao intent service
        intent.putExtra("action", action);
        intent.putExtra("song", baiHat.getUri());// gui Id bai hat sang Mediaplayer
        mActivity.startService(intent);//chay  Intentservicer  sang Mediaplayer*/
    }

    @Override
    public void onPause(BaiHat baiHat) {
        onActionService("pause",baiHat);
    }

    @Override
    public void onPlay(BaiHat baiHat) {
        onActionService("play",baiHat);
    }
    @Override
    public void onGetTime(BaiHat baiHat) {
        onActionService("time",baiHat);
    }
    @Override
    public void seekTo(BaiHat baiHat, int time) {
        Intent intent = new Intent(mActivity, MediaPlaybackService.class);  // khoi tao intent service
        intent.putExtra("song", baiHat.getUri());// gui Id bai hat sang Mediaplayer
        intent.putExtra("action", "goto");
        intent.putExtra("timeTo", time);
        mActivity.startService(intent);//chay  Intentservicer  sang Mediaplayer*/
    }

    @Override
    public void onNext(int positionActive) {
        mActivity.stopService(new Intent(mActivity,MediaPlaybackService.class));//
        onActionService("new",listBaiHat.get(positionActive));
    }

    @Override
    public void onPrev(int positionActive) {
        mActivity.stopService(new Intent(mActivity,MediaPlaybackService.class));//
        onActionService("new",listBaiHat.get(positionActive));
    }
    @Override
    public void start(int positionActive){
        mActivity.stopService(new Intent(mActivity,MediaPlaybackService.class));//
        onActionService("new",listBaiHat.get(positionActive));
    }
    @Override
    public void stop(){
        mActivity.stopService(new Intent(mActivity,MediaPlaybackService.class));
    }
    @Override
    public void setListMusic(ArrayList<BaiHat> list) {
        this.listBaiHat = list;
        MyNotification.listBaiHat = list;

    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}
