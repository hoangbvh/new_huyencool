package huyencool.com.music2.controller;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import huyencool.com.music2.MyNotification;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.view.MediaPlaybackFragment;
import huyencool.com.music2.R;
import huyencool.com.music2.view.AllSongFragment;
import huyencool.com.music2.view.new_interface.AllSongView;
import huyencool.com.music2.view.new_interface.ViewPlayerListener;

public class LandscapeController extends LayoutController {

    public LandscapeController(AppCompatActivity activity) {
        super(activity);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mActivity.getSupportFragmentManager().putFragment(outState,"allSong",allSongFragment);
    }

    @Override
    public void onCreate(Bundle saveInstanceState, String title) {
        if(mActivity.findViewById(R.id.fragmentAllSong) != null)
        {
            if (saveInstanceState != null){
                allSongFragment = (AllSongFragment) mActivity.getSupportFragmentManager().getFragment(saveInstanceState,"allSong");
                if (allSongFragment == null){
                    allSongFragment = new AllSongFragment();
                }
            }
            else{
                allSongFragment = new AllSongFragment();

            }
            allSongFragment.setAllSongView(this);
            allSongFragment.setListMusicListener(this);
            mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fragmentAllSong,allSongFragment).commit();
        }
    }
    @Override
    public void onItemMusicClickListener(BaiHat baiHat) {
        baiHatActive = baiHat;
        startService(baiHat);
    }

    @Override
    public void showMediaPlaybackPlayer(BaiHat baiHat, int pos) {

        mediaPlaybackFragment = new MediaPlaybackFragment();
        mediaPlaybackFragment.setViewPlayerListener(viewPlayerListener);
        mediaPlaybackFragment.setMusicPlayListener(this);
        mediaPlaybackFragment.setPositionActive(pos);
        mediaPlaybackFragment.setListBaiHat(listBaiHat);
        mediaPlaybackFragment.setBaiHatActive(baiHat);
        MyNotification.callBackHelper = mediaPlaybackFragment;
        mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fragmentMediaPlayback,mediaPlaybackFragment).commit();
    }

    @Override
    public void setViewPlayerListener(ViewPlayerListener viewPlayerListener) {
        this.viewPlayerListener = viewPlayerListener;
    }

    @Override
    public void updateMediaFragment(BaiHat baiHat) {
        mediaPlaybackFragment.updateUI(baiHat);
    }
}
